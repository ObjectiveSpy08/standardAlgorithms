def binary_search(a, n, low, high)
  return false if low > high
  mid = (low + high) / 2
  if n == a[mid]
    return true
  elsif n > a[mid]
    return binary_search(a, n, mid + 1, high)
  else
    return binary_search(a, n, low, mid - 1)
  end
end
def main
  puts "Enter the numbers in sorted order:"
  a = gets.chomp.split.map(&:to_i)
  puts "Enter the number to search:"
  n = gets.chomp.to_i
  puts binary_search(a, n, 0, a.size - 1) ? "Found" : "Not Found"
end

main