def main
  puts "Enter the numbers in sorted order:"
  a = gets.chomp.split.map(&:to_i)
  puts "Enter the number to search:"
  n = gets.chomp.to_i
  low = 0
  high = a.size - 1
  while low <= high
    mid = (low + high) / 2
    if n == a[mid]
      puts "Found"
      return;
    elsif n < a[mid]
      high = mid - 1
    else
      low = mid + 1
    end
  end
  puts "Not found"
end

main