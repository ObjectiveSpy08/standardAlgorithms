#include<iostream>
using namespace std;
//Program for binary search:needs sorted array
//Complexity:logn(all cases)-This is the best possible for comparison based searching
int main(){
	int i,n,k,first,last,mid;
	bool found=false;
	cin>>n>>k;
	int a[n];
	for(i=0;i<n;i++) cin>>a[i];
	first=a[0];last=a[n-1];
	while(!found && first<=last){
		mid=(first+last)/2;
		if(a[mid]==k) found=true;
		else if(k<a[mid]) last=mid-1;
		else first=mid+1; 
	}
	if(found) cout<<"\nFound";
	else cout<<"\nNot Found";
}