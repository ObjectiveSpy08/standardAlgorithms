package linearSearch;
import java.util.Scanner;
import java.util.stream.IntStream;

public class LinearSearch {
	public static void main(String[] args) {
		int i, k, n;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of numbers: ");
		n = input.nextInt();
		int[] a = new int[n];
		System.out.println("Enter the numbers:");
		for(i = 0; i < n; i++) {
			a[i] = input.nextInt();
		}
		System.out.println("Enter the number to search:");
		k = input.nextInt();
		// for fun. yet to learn java properly.
		boolean contains = IntStream.of(a).anyMatch(x -> x == k);
		System.out.println(contains ? "Found" : "Not found");
		input.close();
	}
}
