#include<iostream>
using namespace std;
bool rec_linear(int a[],int k,int n){
	if(n<0) return false;
	else if(a[0]==k) return true;
	else return rec_linear(a+1,k,--n);

}
int main(){
	int i,n,k;
	bool found;
	cin>>n>>k;
	int arr[n];
	for(i=0;i<n;i++) cin>>arr[i];
	found=rec_linear(arr,k,n);
	if(found) cout<<"\nFound";
	else cout<<"\nNot Found";
}
