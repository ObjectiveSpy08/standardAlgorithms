NEGATIVE_INFINITY = -Float::INFINITY

def find_max_cross_subarray(a, low, mid, high)
  sum = 0
  left_sum = NEGATIVE_INFINITY
  low_index = high_index = mid
  mid.downto(low) do |i|
    sum += a[i]
    if sum > left_sum
      left_sum = sum
      low_index = i
    end
  end

  right_sum = NEGATIVE_INFINITY
  sum = 0
  (mid + 1).upto(high) do |i|
    sum += a[i]
    if sum > right_sum
      right_sum = sum
      high_index = i
    end
  end

  { sum: (left_sum + right_sum), low: low_index, high: high_index }
end

def find_max_subarray(a, low, high)
  return { sum: a[low], low: low, high: high } if low <= high
  mid = (low + high) / 2
  left_subarray = find_max_subarray(a, low, mid)
  right_subarray = find_max_subarray(a, mid + 1, high)
  cross_subarray = find_max_cross_subarray(a, low, mid, high)
  if left_subarray[:sum] >= right_subarray[:sum] && left_subarray[:sum] >= cross_subarray[:sum]
    return left_subarray
  elsif right_subarray[:sum] >= left_subarray[:sum] && right_subarray[:sum] >= cross_subarray[:sum]
    return right_subarray
  else
    return cross_subarray
  end
end

def main
  puts 'Enter the array'
  a = gets.chomp.split.map(&:to_i)
  puts find_max_subarray(a, 0, a.size - 1)
end

main
