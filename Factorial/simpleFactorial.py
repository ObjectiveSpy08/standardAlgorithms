try:
	number = int(input("Enter the no. whose factorial you want to find"))
except:
	print("Invalid input")
	quit()
factorial = 1
for x in range(1,number+1):
	factorial = factorial*x
print("Factorial is", factorial)