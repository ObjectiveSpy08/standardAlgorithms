def main
=begin
  Exponentiation by squaring In the algorithm, we calculate base raised to powers of two and then multiply it with the result whenever there is a “1” in the binary representation of n.
  Whenever there’s a 0 in the binary representation, we simply move to the next power of twoby squaring the previous power of 2 of the base.
=end
  puts "Enter the base: "
  a = gets.chomp.to_i
  puts "Enter the exponent: "
  n = gets.chomp.to_i
  result = 1
  while n > 0
    if n % 2 == 1 then result = result * a end
    n = n / 2
    a = a * a
  end
  puts result
end

main()