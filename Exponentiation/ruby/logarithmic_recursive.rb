def recursive_exponnetiation(a , n)
  # base case:
  if n == 0
    return 1
  end
  return a * recursive_exponnetiation(a*a, n/2) if n % 2 == 1
  return recursive_exponnetiation(a*a, n/2)
end

def main
  puts "Enter the base: "
  a = gets.chomp.to_i
  puts "Enter the exponent: "
  n = gets.chomp.to_i
  puts recursive_exponnetiation(a, n)
end

main