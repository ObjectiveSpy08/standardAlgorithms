def main
  puts "Enter the base: "
  a = gets.chomp.to_i
  puts "Enter the exponent: "
  n = gets.chomp.to_i
  result = 1
  (0...n).each do |i|
    puts i
    result *= a
  end
  puts "Result: #{result}"
end

main()