package exponentiation;
import java.util.Scanner;
import java.math.BigInteger;


public class LogarithmicRecursive {
	public static void main(String[] args) {
		int a, n;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the base: ");
		a = input.nextInt();
		System.out.println("Enter the exponent: ");
		n = input.nextInt();
		System.out.println("Result: " + logarithmicRecursive(new BigInteger(BigInteger.valueOf(a) + ""), n));
		input.close();
	}
	public static BigInteger logarithmicRecursive(BigInteger aBig, int n) {
		if (n == 0) return new BigInteger("1");
		if (n % 2 == 1) {
			return aBig.multiply(logarithmicRecursive(aBig.multiply(aBig), n/2));
		}
		return logarithmicRecursive(aBig.multiply(aBig), n/2);
	}
}
