package exponentiation;

import java.util.Scanner;
import java.math.BigInteger;

public class LogarithmicNonRecursive {
	public static void main(String[] args) {
		int a, n;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the base: ");
		a = input.nextInt();
		System.out.println("Enter the exponent: ");
		n = input.nextInt();
		BigInteger aBig = new BigInteger(BigInteger.valueOf(a) + "");
		BigInteger result = new BigInteger("1");
		while (n > 0) {
			if (n % 2 == 1) {
				result = result.multiply(aBig);
			}
			n = n / 2;
			aBig = aBig.multiply(aBig);

		}
		System.out.println(result);
		input.close();
	}
}
