package exponentiation;

import java.math.BigInteger;
import java.util.Scanner;

public class Linear {
	public static void main(String[] args) {
		int a,n;
		BigInteger result = new BigInteger("1");
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the base: ");
		a = input.nextInt();
		System.out.println("Enter the exponent: ");
		n = input.nextInt();
		for (int i = 0; i < n; i++) {
			result = result.multiply(BigInteger.valueOf(a));
		}
		System.out.println("Result is: " + result);
		input.close();
	}
}
