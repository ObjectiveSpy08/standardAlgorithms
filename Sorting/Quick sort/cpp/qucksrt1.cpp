#include<iostream>
using namespace std;
void quicksort(int a[],int low,int high){
    int pivot=a[low],i=low,j=high+1,t;
    if( low>=high) return;
    while(1){
        while(a[++i]<pivot) if(i==high) break;
        while(a[--j]>pivot);
        if(i>=j) break;
        t=a[i];a[i]=a[j];a[j]=t;
    }
    t=a[low];a[low]=a[j];a[j]=t;
    quicksort(a,j+1,high);
    quicksort(a,low,j-1);
}
int main(){
    int i,n;
    cin>>n;
    int a[n];
    for(i=0;i<n;i++) cin>>a[i];
    quicksort(a,0,n-1);
    cout<<"\n";
    for(i=0;i<n;i++) cout<<" "<<a[i];
}
