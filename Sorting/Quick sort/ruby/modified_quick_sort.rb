def partition(a, left, right)
  i = left - 1
  pivot = a[right]
  left.upto(right - 1) do |j|
     if a[j] < pivot
       i += 1
       a[j], a[i] = a[i], a[j]
     end
  end
  a[i + 1], a[right] = a[right], a[i + 1]
  i + 1
end

def quick_sort(a, left, right)
  if left >= right
    return
  end
  pivot = partition(a, left, right)
  quick_sort(a, left, pivot - 1)
  quick_sort(a, pivot + 1, right)
end

def main
  puts "Enter the numbers: "
  a = gets.chomp.split.map(&:to_i)
  quick_sort(a, 0, a.size - 1)
  puts a
end

main