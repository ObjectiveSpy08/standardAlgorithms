def merge_sort(a, b, left, right)
  if left >= right
    return
  end
  middle = (left + right) / 2
  merge_sort(a, b, left, middle)
  merge_sort(a, b, middle + 1, right)
  merge(a, b, left, right)
end

def merge(a, b, left, right)
  m = (left + right) / 2
  b_index = left
  left_index = left
  left_last = m
  right_index = m + 1
  right_last = right
  while left_index <= left_last && right_index <= right_last
    if a[left_index] <= a[right_index]
      b[b_index] = a[left_index]
      left_index += 1
    else
      b[b_index] = a[right_index]
      right_index += 1
    end
    b_index += 1
  end
  while left_index <= m
    b[b_index] = a[left_index]
    b_index += 1
    left_index += 1
  end
  while right_index <= right
    b[b_index] = a[right_index]
    b_index += 1
    right_index += 1
  end
  while left < b_index
    a[left] = b[left]
    left += 1
  end
end

def main
  puts 'Enter the numbers: '
  a = gets.chomp.split.map(&:to_i)
  puts a[0].is_a? String
  puts "initialization"
  merge_sort(a, Array.new(a.size), 0, a.size - 1)
  puts "Sorted: #{a}"
end

main