#include<iostream>
using namespace std;
//Program for merge sort using linked lists:time complexity-nlogn(best possible); space complexity-logn
struct node{
	int data;
	node *next;
};
node* create_list(){
	int t,n;//no. of nodes-just for taking input easily...not availble for implementing the algo
	node *head=NULL,*last,*temp;
	cin>>n;
	while(n--){
		temp=new node;
		cin>>t;
		temp->data=t;
		temp->next=NULL;
		if(head==NULL) head=last=temp;
		else {
			last->next=temp;
			last=last->next;
		}
	}
	return head;
}
void divide_list(node *first1,node* &first2){
	if(first1==NULL || first1->next==NULL) first2=NULL;
	else{
		node *curr=first1->next,*middle=first1;
		if(curr!=NULL) curr=curr->next;
		while(curr!=NULL){
			curr=curr->next;
			middle=middle->next;
			if(curr!=NULL) curr=curr->next;
		}
		first2=middle->next;
		middle->next=NULL;
	}
}
node *merge_list(node *first1,node *first2){
	node *newhead,*lastsmall;
	if(first1==NULL) return first2;
	else if(first2==NULL) return first1;
	else{
			if(first1->data<first2->data){
			newhead=lastsmall=first1;
			first1=first1->next;
		}
		else{
			newhead=lastsmall=first2;
			first2=first2->next;
		}
		while(first1!=NULL && first2!=NULL){
			if(first1->data<first2->data){
				lastsmall->next=first1;
				first1=first1->next;
				lastsmall=lastsmall->next;
			}
			else{
				lastsmall->next=first2;
				first2=first2->next;
				lastsmall=lastsmall->next;
			}
		}
		if(first1==NULL) lastsmall->next=first2;
		else lastsmall->next=first1;
		return newhead;
	}
}
void merge_sort(node* &head){
	node *otherhead;
	if(head!=NULL && head->next!=NULL){
		divide_list(head,otherhead);
		merge_sort(head);
		merge_sort(otherhead);
		head=merge_list(head,otherhead);
	}
}
int main(){
	node *curr,*head=create_list();
	merge_sort(head);
	curr=head;
	while(curr!=NULL){
		cout<<"\n"<<curr->data;
		curr=curr->next;
	}
}
