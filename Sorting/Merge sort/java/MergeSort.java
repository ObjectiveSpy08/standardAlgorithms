package mergeSort;

import java.util.Scanner;

public class MergeSort {
	
	public static void merge(int[] a, int[] b, int left, int right) {
		int middle = (left + right)/2;
		int leftIndex = left;
		int bIndex = left;
		int leftLast = middle;
		int rightIndex = middle + 1;
		int rightLast = right;
		while(leftIndex <= leftLast && rightIndex <= rightLast) {
			if(a[leftIndex] <= a[rightIndex]) {
				b[bIndex] = a[leftIndex];
				leftIndex++;
			}
			else {
				b[bIndex] = a[rightIndex];
				rightIndex++;
			}
			bIndex++;
		}
		while(leftIndex <= leftLast) {
			b[bIndex] = a[leftIndex];
			leftIndex++;
			bIndex++;
		}
		while(rightIndex <= rightLast) {
			b[bIndex] = a[rightIndex];
			rightIndex++;
			bIndex++;
		}
		while(left <= right) {
			a[left] = b[left];
			left++;
		}
	}
	
	public static void mergeSort(int[] a, int[] b, int left, int right) {
		if(left >= right) return;
		int middle = (left+right)/2;
		mergeSort(a, b , left, middle);
		mergeSort(a, b, middle + 1, right);
		merge(a, b, left, right);
	}
	
	public static void main(String[] args) {
		int i, n;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of numbers: ");
		n = input.nextInt();
		int[] a = new int[n];
		System.out.println("Enter the numbers: ");
		for (i = 0; i < n; i++) {
			a[i] = input.nextInt();
		}
		mergeSort(a, new int[n], 0, n-1);
		System.out.println("Sorted: " );
		for (i = 0; i < n; i++) {
			System.out.println(a[i]);
		}
		input.close();
	}

}
