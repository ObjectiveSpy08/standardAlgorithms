#include<iostream>
using namespace std;
//Program for bubble sort;O(n2) all cases
//Also called sink sort(another variant in which largest element if pushed to last)
int main(){
    int i,j,n;
    bool swapped=false;
    cin>>n;
    int a[n];
    for(i=0;i<n;i++) cin>>a[i];
    for(i=0;i<n;i++){
        for(j=n-1;j>i;j--){
            if(a[j]<a[j-1]){
              swap(a[j],a[j-1]);
              swapped=true;
            }
        }
        if(!swapped) break;
    }
    for(i=0;i<n;i++) cout<<" "<<a[i];
}
