#include<iostream>
using namespace std;
//Prg for sink sort variant of bubble sort
int main(){
    int i,j,n;
    bool swapped=false;
    cin>>n;
    int a[n];
    for(i=0;i<n;i++) cin>>a[i];
    for(i=0;i<n;i++){
        for(j=0;j<n-1-i;j++){
            if(a[j]>a[j+1]){
                swap(a[j],a[j+1]);
                swapped=true;
            }
        }
        if(!swapped) break;
    }
    for(i=0;i<n;i++) cout<<a[i];
}
