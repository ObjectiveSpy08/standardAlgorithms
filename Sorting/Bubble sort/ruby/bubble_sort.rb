def main
  puts "Enter the numbers: "
  a = gets.chomp.split.map(&:to_i)
  for i in 0...a.length
    for j in 0...a.length-i-1
      if a[j] > a[j+1]
        a[j], a[j+1] = a[j+1], a[j]
      end
    end
  end
  puts a
end

main