package bubbleSort;

import java.util.Scanner;

public class BubbleSort {
	public static void main(String[] args) {
		int i, j, n;
		System.out.println("Enter the number of numbers: ");
		Scanner input = new Scanner(System.in);
		n = input.nextInt();
		int[] a = new int[n];
		System.out.println("Enter the numbers: ");
		for (i = 0; i < n; i++) {
			a[i] = input.nextInt();
		}
		for (i = 0; i < n; i++) {
			for (j = 0; j < n - i - 1; j++) {
				// Just another way to swap (ways: temp var, +/-, *//, ^(XOR))
				if (a[j] > a[j + 1]) {
					a[j] = a[j] ^ a[j + 1];
					a[j + 1] = a[j] ^ a[j + 1];
					a[j] = a[j] ^ a[j + 1];
				}
			}
		}
		System.out.println("Sorted numbers: ");
		for (i = 0; i < n; i++) {
			System.out.printf("%d ", a[i]);
		}
		input.close();
	}

}
