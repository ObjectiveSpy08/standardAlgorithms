package selection_sort;

import java.util.Scanner;

public class SelectionSort {
	public static void main(String[] args) {
		int i, j, n;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of numebers");
		n = input.nextInt();
		int[] a = new int[n];
		System.out.println("Enter the numbers: ");
		for (i = 0; i < n; i++) {
			a[i] = input.nextInt();
		}
		for (i = 0; i < n - 1; i++) {
			for (j = i + 1; j < n; j++) {
				if (a[i] > a[j]) {
					a[i] += a[j];
					a[j] = a[i] - a[j];
					a[i] -= a[j];
				}
			}
		}
		System.out.println("Sorted numbers: ");
		for (i = 0; i < n; i++) {
			System.out.println(a[i]);
		}
		input.close();

	}
}
