def main
  puts "Enter the numbers: "
  a = gets.chomp.split.map(&:to_i)
  for i in 0...a.length - 1
    for j in i+1...a.length
      if a[i] > a[j]
        a[i], a[j] = a[j], a[i]
      end
    end
  end
  puts a
end

main