#include<iostream>
using namespace std;
//Prg for selection sort; time complexity:O(n2) all cases
int main(){
    int i,j,k,n;
    cin>>n;
    int a[n];
    for(i=0;i<n;i++) cin>>a[i];
    for(i=0;i<n-1;i++){
        k=i;
        for(j=i+1;j<n;j++){
            if(a[j]<a[k]) k=j;
        }
        swap(a[i],a[k]);
    }
    for(i=0;i<n;i++) cout<<" "<<a[i];
}
