def main
  puts 'Enter the numbers, space separated'
  a = gets.chomp.split.map(&:to_i)
  for i in 1...a.length
    key = a[i]
    j = i-1
    while j >= 0 && a[j] > key
      a[j + 1] = a[j]
      j -= 1
    end
    a[j + 1] = key
  end
  puts a
end

main
