package insertionSort;

import java.util.Scanner;

public class InsertionSort {
	public static void main(String[] args) {
		int i, j, key, n;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of numbers: ");
		n = input.nextInt();
		int[] a = new int[n];
		System.out.println("Enter the numbers: ");
		for (i = 0; i < n; i++) {
			a[i] = input.nextInt();
		}
		for (i = 1; i < n; i++) {
			key = a[i];
			for (j = i - 1; j >= 0 && a[j] > key; j--) {
				a[j + 1] = a[j];
			}
			a[j + 1] = key;
		}
		System.out.println("Sorted numbers: ");
		for (i = 0; i < n; i++) {
			System.out.println(a[i] + " ");
		}
		input.close();
	}

}
