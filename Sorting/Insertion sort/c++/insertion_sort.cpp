#include <iostream>
using namespace std;

// Optimal insertion sort w/o modifications

int main()
{
    int i, j, key, n;
    cout << "Enter number of numbers: ";
    cin >> n;
    int a[n];
    cout << "\nEnter the numbers: ";
    for (i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    for (i = 1; i < n; i++)
    {
        key = a[i];
        for (j = i - 1; j >= 0 && a[j] > key; j--)
        {
            a[j + 1] = a[j];
        }
        a[j + 1] = key;
    }

    for (i = 0; i < n; i++)
    {
        cout << a[i] << " ";
    }
}