def max_heapify(a, index, heap_size)
  largest = index
  left_child_index = 2 * index + 1
  right_child_index = 2 * index + 2
  if left_child_index < heap_size && a[left_child_index] > a[index]
    largest = left_child_index
  end
  if right_child_index < heap_size && a[right_child_index] > a[largest]
    largest = right_child_index
  end
  if largest != index
    a[index], a[largest] = a[largest], a[index]
    max_heapify(a, largest, heap_size)
  end
end

def build_max_heap(a)
  heap_size = a.size
  (a.size / 2 - 1).downto(0) do |i|
    max_heapify(a, i, heap_size)
  end
end

def heap_sort(a)
  build_max_heap(a)
  heap_size = a.size
  (a.size - 1).downto(1) do |i|
    a[0], a[i] = a[i], a[0]
    heap_size = heap_size - 1
    max_heapify(a, 0, heap_size)
  end
  a
end

def main
  puts "Enter the numbers: "
  puts heap_sort(gets.chomp.split.map(&:to_i))
end

main