def counting_sort(a, k)
  # array index starts with 0, so to store a number in c at its corresponding value-index
  # make c of length k + 1
  c = Array.new(k + 1, 0)
  b = Array.new(a.length)
  a.each do |x|
    c[x] += 1
  end
  for i in (1...c.length)
    c[i] += c[i-1]
  end
  (a.length - 1).downto(0) do |i|
    # we are storing the last instance of a number first. Like if there are two 1s, we store the second one first.
    # However the index of that number will be one less than the total count of that number in a, as we start indexing from 0.
    b[c[a[i]] - 1] = a[i]
    c[a[i]] -= 1
  end
  b
end

def main
  puts "Enter the numbers:"
  a = gets.chomp.split.map(&:to_i)
  k = a.max
  puts counting_sort(a, k)
end

main